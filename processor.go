package main
//https://ednsquare.com/story/how-to-make-http-requests-in-golang------5VIjL3
import(
	"bytes"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"time"
)



func push(values []interface{})(bool){
	//t:=time.Now()
	//
	//var baseurl string="http://139.162.7.137:9200/";
	//var message=map[string]interface{}{
	//	"hostname":"sachin",
	//	"rtt":2.34,
	//	"@timestamp":t,
	//}
	var baseurl string="http://139.162.7.137:9200/pttest/_doc/";
	for _,v:= range values{
		message:=v.(map[string]interface{})
		req,_:=json.Marshal(message["_source"]);
		resp,err:=http.Post(baseurl,"application/json",bytes.NewReader(req));
		defer resp.Body.Close()
		if(err!=nil){
			log.Fatal("err",err);
		}
		var result map[string]interface{}
		if(resp.StatusCode!=201){
			log.Println(resp.StatusCode,resp.Status)
			return (false)
		}
		json.NewDecoder(resp.Body).Decode(&result)
	}
	return true
}

func pull()(bool,[]interface{}){
	baseurl:="http://localhost:9200/ptbeat-8.0.0/_search?pretty=true&filter_path=hits.hits._source.rtt,hits.hits._source.@timestamp,hits.hits._source.host.hostname&_source=rtt,@timestamp,host&sort=@timestamp:desc&size=5" // change this url if modifying ptbeat
	resp,err:=http.Get(baseurl);
	if(err!=nil){
		log.Fatal("err",err)
	}
	defer resp.Body.Close()
	var result map[string]interface{}
	if(err!=nil){
		log.Fatal(err)
	}
	json.NewDecoder(resp.Body).Decode(&result)
	d:=result["hits"]
	z:=d.(map[string]interface{})["hits"]
	nv:=z.([]interface{})//array of values from the local storage
	return true,nv;
}

func main(){
	fmt.Println("starting")
	isworkinglocal:=true;
	isworkingcloud:=true;
	var value []interface{}
	for isworkinglocal && isworkingcloud{
		isworkinglocal,value=pull();
		fmt.Println(value)
		isworkingcloud=push(value);
		time.Sleep(5*time.Second)
	}
}